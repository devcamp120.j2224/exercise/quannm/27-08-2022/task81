$(document).ready(function() {
    $('#btn-login').on('click', onBtnLoginClick);
    var gUrl = "http://42.115.221.44:8080/devcamp-auth/users/signin?email=";
    function onBtnLoginClick() {
        var vSignInObj = {
            email: "",
            password: ""
        };
        getDataFromInput(vSignInObj);
        // console.log(vSignInObj);
        var vIsCheck = validateData(vSignInObj);
        if (vIsCheck) {
            callApiSignInAccount(vSignInObj);
        }
    }
    function getDataFromInput(paramObj) {
        paramObj.email = $('#inp-email').val().trim();
        paramObj.password = $('#inp-password').val().trim();
    }
    function validateData(paramObj) {
        if (!validateEmail(paramObj.email)) {
            alert("Email không phù hợp");
            return false;
        }
        if (paramObj.password === "") {
            alert("Password không phù hợp");
            return false;
        }
        return true;
    }
    function validateEmail(paramEmail) {
        const regex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return regex.test(String(paramEmail).toLowerCase());
    }
    function callApiSignInAccount(paramObj) {
        $.ajax({
            url: gUrl + paramObj.email + '&password=' + paramObj.password,
            type: 'POST',
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (pRes) {
                console.log(pRes);
                setCookie("token", pRes.token, 1);
                window.location.href = "userInfo.html";
            },
            error: function (pAjaxContext) {
                console.log(pAjaxContext.responseText);
            }
        });
    }
    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }
});