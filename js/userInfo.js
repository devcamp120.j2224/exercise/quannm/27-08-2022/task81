$(document).ready(function () {
    const token = getCookie("token");
    var headers = {
        Authorization: "Bearer " + token
    };
    var gUrl = "http://42.115.221.44:8080/devcamp-auth/users/me";
    onPageLoading();
    $("#btn-logout").on("click", redirectToLogin);
    function onPageLoading() {
        callApiGetUserInfo();
    }
    function callApiGetUserInfo() {
        $.ajax({
            url: gUrl,
            method: "GET",
            headers: headers,
            async: false,
            success: function(responseObject) {
                console.log(responseObject);
                displayUser(responseObject);
            },
            error: function(xhr) {
                console.log(xhr);
                redirectToLogin()
            }
        });
    }
    function redirectToLogin() {
        setCookie("token", "", 1);
        window.location.href = "login.html";
    }
    function displayUser(data) {
        $("#inp-first-name").val(data.firstname);
        $("#inp-last-name").val(data.lastname);
        $("#inp-email").val(data.email);
    };
    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }
})