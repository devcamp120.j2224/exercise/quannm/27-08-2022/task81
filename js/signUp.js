$(document).ready(function() {
    $('#btn-sign-up').on('click', onBtnSignUpClick);
    var gUrl = "http://42.115.221.44:8080/devcamp-auth/users/signup";
    function onBtnSignUpClick() {
        var vSignUpObj = {
            firstname: "",
            lastname: "",
            email: "",
            password: ""
        };
        var vConfirmPassword = $('#inp-confirm-password').val().trim();;
        getDataFromInput(vSignUpObj);
        // console.log(vSignUpObj);
        // console.log(vConfirmPassword);
        var vIsCheck = validateData(vSignUpObj, vConfirmPassword);
        if (vIsCheck) {
            // console.log(vSignUpObj, vConfirmPassword);
            callApiSignUpAccount(vSignUpObj);
        }
    }
    function getDataFromInput(paramObj) {
        paramObj.firstname = $('#inp-first-name').val().trim();
        paramObj.lastname = $('#inp-last-name').val().trim();
        paramObj.email = $('#inp-email').val().trim();
        paramObj.password = $('#inp-password').val().trim();
    }
    function validateData(paramObj, paramConfirm) {
        var vCheckBox = $('#inp-check-accept');
        if (!validateEmail(paramObj.email)) {
            alert("Email không phù hợp");
            return false;
        }
        if (paramObj.firstname === "") {
            alert("Firstname không phù hợp");
            return false;
        }
        if (paramObj.lastname === "") {
            alert("Lastname không phù hợp");
            return false;
        }
        if (paramObj.password === "") {
            alert("Password không phù hợp");
            return false;
        }
        if (paramConfirm != paramObj.password) {
            alert("Password không trùng khớp");
            return false;
        }
        if (!vCheckBox.is(":checked")) {
            alert("Bạn chưa đồng ý điều khoản người dùng");
            return false;
        }
        return true;
    }
    function validateEmail(paramEmail) {
        const regex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return regex.test(String(paramEmail).toLowerCase());
    }
    function callApiSignUpAccount(paramObj) {
        $.ajax({
            url: gUrl,
            type: 'POST',
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(paramObj),
            success: function (pRes) {
                console.log(pRes)
            },
            error: function (pAjaxContext) {
                alert(pAjaxContext.responseJSON.message);
            }
        });
    }
});